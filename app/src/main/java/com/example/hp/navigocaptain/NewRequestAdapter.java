package com.example.hp.navigocaptain;

import android.content.Context;
import android.location.Location;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class NewRequestAdapter extends RecyclerView.Adapter<NewRequestAdapter.NewRequestViewHolder> {

    private FirebaseAuth auth = FirebaseAuth.getInstance();
    private FirebaseUser currentUser = auth.getCurrentUser();
    private DatabaseReference mdbRef = FirebaseDatabase.getInstance().getReference();
    private Context context;
    private List<NewRequests> newRequestsList;

    public NewRequestAdapter(Context context, List<NewRequests> newRequestsList) {
        this.context = context;
        this.newRequestsList = newRequestsList;
    }

    @NonNull
    @Override
    public NewRequestAdapter.NewRequestViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.new_requests,parent,false);
        return new NewRequestViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final NewRequestAdapter.NewRequestViewHolder holder, int i) {

        final NewRequests newRequests = newRequestsList.get(i);
        holder.Name.setText(newRequests.getName());
        holder.Time.setText(newRequests.getTime());
        holder.Message.setText(newRequests.getMessage());
        Picasso.get()
                .load(newRequests.getPhoto())
                .placeholder(R.drawable.user)
                .error(R.drawable.user)
                .into(holder.Image);

        mdbRef.child("RealTimePosition").child(currentUser.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.child("Latitude").exists() && dataSnapshot.child("Longitude").exists()) {
                    double plat = (double) dataSnapshot.child("Latitude").getValue();
                    double plng = (double) dataSnapshot.child("Longitude").getValue();
                    // Calculating distance between customer and provider
                    Location startPoint = new Location("Provider");
                    startPoint.setLatitude(plat);
                    startPoint.setLongitude(plng);

                    Location endPoint = new Location("Customer");
                    endPoint.setLatitude(newRequests.getLatitude());
                    endPoint.setLongitude(newRequests.getLongitude());

                    DecimalFormat decimalFormat = new DecimalFormat("#0.0");
                    double distance = startPoint.distanceTo(endPoint);
                    String distanceInUnit = decimalFormat.format(distance/1000) + " km";
                    holder.Distance.setText(distanceInUnit);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        String status = newRequests.getStatus();
        final String ckey = newRequests.getCkey();

        holder.Distance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String tdistance = holder.Distance.getText().toString();
                Toast.makeText(context,newRequests.getName() + " is " + tdistance + " away from your current position", Toast.LENGTH_LONG).show();
            }
        });

        holder.rejectCust.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mdbRef.child("Orders").child(currentUser.getUid()).child(ckey).child("Status").setValue("-1");
            }
        });

        holder.acceptCust.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mdbRef.child("Users").child(currentUser.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.child("TasksLeft").exists()) {
                            int tasksleft = Integer.parseInt(dataSnapshot.child("TasksLeft").getValue().toString());
                            if (tasksleft <= 0) {
                                Toast.makeText(context,"Operation Denied", Toast.LENGTH_LONG).show();
                                return;
                            }
                            String newtasksleft = String.valueOf(tasksleft - 1);
                            mdbRef.child("Users").child(currentUser.getUid()).child("TasksLeft").setValue(newtasksleft);
                            mdbRef.child("Orders").child(currentUser.getUid()).child(ckey).child("Status").setValue("1");
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
        });

    }

    @Override
    public int getItemCount() {
        return newRequestsList.size();
    }

    public class NewRequestViewHolder extends RecyclerView.ViewHolder {

        public CircleImageView Image;
        public TextView Name;
        public TextView Time;
        public TextView Message;
        public TextView Distance;
        public Button acceptCust, rejectCust;

        public NewRequestViewHolder(@NonNull View itemView) {
            super(itemView);

            Image = (CircleImageView) itemView.findViewById(R.id.customerPic);
            Name = (TextView) itemView.findViewById(R.id.customerName);
            Time = (TextView) itemView.findViewById(R.id.time);
            Distance = (TextView) itemView.findViewById(R.id.distance);
            Message = (TextView) itemView.findViewById(R.id.custMsg);
            acceptCust = (Button) itemView.findViewById(R.id.acceptCust);
            rejectCust = (Button) itemView.findViewById(R.id.rejectCust);
        }
    }
}
