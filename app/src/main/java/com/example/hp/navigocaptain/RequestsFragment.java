package com.example.hp.navigocaptain;


import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.squareup.picasso.Picasso;

import java.lang.ref.ReferenceQueue;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class RequestsFragment extends Fragment {


    private FirebaseAuth auth;
    private FirebaseUser currentUser;
    private DatabaseReference mdbRef;

    private ImageView icon;
    private TextView text;

    private RecyclerView recyclerView;
    private List<NewRequests> newRequestsList;
    private NewRequestAdapter newRequestAdapter;

    public RequestsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_requests, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();

        auth = FirebaseAuth.getInstance();
        currentUser = auth.getCurrentUser();
        mdbRef = FirebaseDatabase.getInstance().getReference();

        recyclerView = (RecyclerView) getView().findViewById(R.id.RecyclerView);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        newRequestsList = new ArrayList<>();

        icon = (ImageView) getView().findViewById(R.id.icon);
        text = (TextView) getView().findViewById(R.id.Activetext);

        mdbRef.child("Orders").child(currentUser.getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    newRequestsList.clear();
                    recyclerView.setVisibility(View.VISIBLE);
                    icon.setVisibility(View.GONE);
                    text.setVisibility(View.GONE);
                    for (DataSnapshot soloRequest : dataSnapshot.getChildren()) {
                        if(soloRequest.child("Status").exists()){
                            if (soloRequest.child("Status").getValue().toString().equals("0"))
                            {
                                NewRequests newRequests = soloRequest.getValue(NewRequests.class);
                                newRequestsList.add(newRequests);
                            }
                        }
                    }

                    newRequestAdapter = new NewRequestAdapter(getActivity(),newRequestsList);
                    recyclerView.setAdapter(newRequestAdapter);
                }
                else {
                    recyclerView.setVisibility(View.GONE);
                    icon.setVisibility(View.VISIBLE); text.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

}
