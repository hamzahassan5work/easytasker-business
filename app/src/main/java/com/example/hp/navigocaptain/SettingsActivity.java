package com.example.hp.navigocaptain;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.DhcpInfo;
import android.net.Uri;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthRecentLoginRequiredException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.angmarch.views.NiceSpinner;
import org.angmarch.views.OnSpinnerItemSelectedListener;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import de.hdodenhof.circleimageview.CircleImageView;
import id.zelory.compressor.Compressor;

public class SettingsActivity extends AppCompatActivity {

    private static final String TAG = "HomeFragment";
    private static final int ERROR_DIALOG_REQUEST = 9001;

    private CircleImageView updateImageView;
    private Button getStarted, setLocation;
    private ProgressBar progressBar;
    private CheckBox locationcheckbox;

    private FirebaseAuth Auth;
    private FirebaseUser currentUser;
    private StorageReference mStorageRef;
    private DatabaseReference mdatabaseRef;

    private Uri imageUri;
    private String profileImageURL ;

    private EditText name, desc;
    private TextView deleteAccount;
    private NiceSpinner servicetype;
    private String mType = "Mechanic";
    private String verificationId;
    private Bitmap compressedBitmap;
    private byte[] compressByte;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        Auth = FirebaseAuth.getInstance();
        currentUser = Auth.getCurrentUser();
        mStorageRef = FirebaseStorage.getInstance().getReference();
        mdatabaseRef = FirebaseDatabase.getInstance().getReference();

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        updateImageView = (CircleImageView) findViewById(R.id.updateUserPic);
        locationcheckbox = (CheckBox) findViewById(R.id.locationcheckbox);
        name = (EditText) findViewById(R.id.username);
        desc = (EditText) findViewById(R.id.userdesc);
        servicetype = (NiceSpinner) findViewById(R.id.usertype);
        deleteAccount = (TextView) findViewById(R.id.deleteaccount);
        setLocation = (Button) findViewById(R.id.setLocation);
        getStarted = (Button) findViewById(R.id.getstarted);

        List<String> dataset = new LinkedList<>(Arrays.asList("Mechanic", "Plumber", "Electrician", "Cleaning", "Gardener", "Painter", "Carpenter", "Medical", "Education", "Other"));
        servicetype.attachDataSource(dataset);

        servicetype.setOnSpinnerItemSelectedListener(new OnSpinnerItemSelectedListener() {
            @Override
            public void onItemSelected(NiceSpinner parent, View view, int position, long id) {
                mType = parent.getItemAtPosition(position).toString();
            }
        });

        CheckLocationSelected();

        updateImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });

        setLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isServicesOK()) {
                    startActivity(new Intent(SettingsActivity.this, MapsActivity.class));
                }
            }
        });

        desc.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                saveProfile();
                return false;
            }
        });
        getStarted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveProfile();
            }
        });

        deleteAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AllInputsEnabled(false);
                progressBar.setVisibility(View.VISIBLE);
                DeleteMyAccount();
            }
        });

    }

    private void DeleteMyAccount() {
        currentUser.delete().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    mdatabaseRef.child("StaticLocation").child(currentUser.getUid()).removeValue();
                    Toast.makeText(SettingsActivity.this,"Account Deleted", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(SettingsActivity.this, PhoneActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                }
                else if (task.getException() instanceof FirebaseAuthRecentLoginRequiredException) {
                    sendVerificationCode(currentUser.getPhoneNumber());
                }
                else {
                    progressBar.setVisibility(View.GONE);
                    AllInputsEnabled(true);
                    Toast.makeText(SettingsActivity.this,task.getException().getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void selectImage() {
        CropImage.activity().setGuidelines(CropImageView.Guidelines.ON).setAspectRatio(1,1).start( this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                imageUri = result.getUri();
                updateImageView.setImageURI(imageUri);
                File file = new File(imageUri.getPath());
                try {
                    compressedBitmap = new Compressor(SettingsActivity.this).compressToBitmap(file);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                compressedBitmap.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream);
                compressByte = byteArrayOutputStream.toByteArray();
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

    private void saveProfile() {

        if(imageUri == null)
        {
            Toast.makeText(SettingsActivity.this,"Choose Profile Image", Toast.LENGTH_LONG).show();
            return;
        }
        if(TextUtils.isEmpty(name.getText()))
        {
            name.setError("Name is required");
            name.requestFocus();
            return;
        }
        if (!locationcheckbox.isChecked()) {
            Toast.makeText(SettingsActivity.this,"Select your service location", Toast.LENGTH_LONG).show();
            return;
        }
        if(TextUtils.isEmpty(desc.getText()))
        {
            desc.setError("Description is required");
            desc.requestFocus();
            return;
        }

        progressBar.setVisibility(View.VISIBLE);
        AllInputsEnabled(false);
        mStorageRef.child("UserProfilePics").child(currentUser.getUid()).putBytes(compressByte).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                if(task.isSuccessful()) {
                    task.getResult().getStorage().getDownloadUrl().addOnCompleteListener(new OnCompleteListener<Uri>() {
                        @Override
                        public void onComplete(@NonNull Task<Uri> task) {
                            if (!TextUtils.isEmpty(task.getResult().toString())) {
                                profileImageURL = task.getResult().toString();
                                ProfileUpdate();
                            }
                            else {
                                progressBar.setVisibility(View.GONE);
                                AllInputsEnabled(true);
                                Toast.makeText(SettingsActivity.this, task.getException().getMessage(), Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                }
                else {
                    progressBar.setVisibility(View.GONE);
                    AllInputsEnabled(true);
                    Toast.makeText(SettingsActivity.this,task.getException().getMessage(),Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    private void ProfileUpdate() {
        UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                .setPhotoUri(Uri.parse(profileImageURL)).setDisplayName(name.getText().toString().trim()).build();

        currentUser.updateProfile(profileUpdates).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(Task<Void> task) {
                if(task.isSuccessful()){
                    UpdateToDatabase();
                }
                else {
                    progressBar.setVisibility(View.GONE);
                    AllInputsEnabled(true);
                    Toast.makeText(SettingsActivity.this,task.getException().getMessage(),Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void UpdateToDatabase() {
        Providers providers = new Providers(currentUser.getPhoneNumber(), name.getText().toString(), profileImageURL);
        final Users users = new Users(name.getText().toString(),desc.getText().toString(),currentUser.getPhoneNumber(),mType,profileImageURL);
        mdatabaseRef.child("Providers").child(mType).child(currentUser.getUid()).setValue(providers).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    mdatabaseRef.child("Users").child(currentUser.getUid()).setValue(users).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                ProviderNode();
                                Intent intent = new Intent(SettingsActivity.this, HomeActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                                finish();
                            }
                            else {
                                progressBar.setVisibility(View.GONE);
                                AllInputsEnabled(true);
                                Toast.makeText(SettingsActivity.this,task.getException().getMessage(),Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                }
                else {
                    progressBar.setVisibility(View.GONE);
                    AllInputsEnabled(true);
                    Toast.makeText(SettingsActivity.this,task.getException().getMessage(),Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void ProviderNode() {
        mdatabaseRef.child("StaticLocation").child(currentUser.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.child("Latitude").exists() && dataSnapshot.child("Longitude").exists()) {
                    double latitude = (double) dataSnapshot.child("Latitude").getValue();
                    double longitude = (double) dataSnapshot.child("Longitude").getValue();
                    mdatabaseRef.child("Types").child(currentUser.getUid()).child("ServiceType").setValue(mType);
                    mdatabaseRef.child("Providers").child(mType).child(currentUser.getUid()).child("Latitude").setValue(latitude);
                    mdatabaseRef.child("Providers").child(mType).child(currentUser.getUid()).child("Longitude").setValue(longitude);
                    mdatabaseRef.child("Users").child(currentUser.getUid()).child("Latitude").setValue(latitude);
                    mdatabaseRef.child("Users").child(currentUser.getUid()).child("Longitude").setValue(longitude);
                    mdatabaseRef.child("Users").child(currentUser.getUid()).child("TasksLeft").setValue("5");
                    mdatabaseRef.child("StaticLocation").child(currentUser.getUid()).removeValue();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void CheckLocationSelected() {
        mdatabaseRef.child("StaticLocation").child(currentUser.getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.child("Latitude").exists() && dataSnapshot.child("Longitude").exists()) {
                    locationcheckbox.setChecked(true);
                    setLocation.setText("Edit");
                }
                else {
                    locationcheckbox.setChecked(false);
                    setLocation.setText("Open Map");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void AllInputsEnabled(boolean value) {
        updateImageView.setEnabled(value);
        name.setEnabled(value);
        name.setFocusable(value);
        name.setFocusableInTouchMode(value);
        desc.setEnabled(value);
        desc.setFocusable(value);
        desc.setFocusableInTouchMode(value);
        servicetype.setEnabled(value);
        setLocation.setEnabled(value);
        getStarted.setEnabled(value);
        deleteAccount.setEnabled(value);
    }

    private boolean isServicesOK() {
        Log.d(TAG,"isServicesOK: Checking google services version");
        int available = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(SettingsActivity.this);

        if(available == ConnectionResult.SUCCESS) {
            Log.d(TAG,"isServicesOK: Google play services is workkng");
            return  true;
        }
        else if(GoogleApiAvailability.getInstance().isUserResolvableError(available)) {
            Log.d(TAG,"isServiceOK: An error occured but we can fix it");
            Dialog dialog = GoogleApiAvailability.getInstance().getErrorDialog(SettingsActivity.this, available, ERROR_DIALOG_REQUEST);
            dialog.show();
        }
        else {
            Toast.makeText(SettingsActivity.this,"You can't make map requests",Toast.LENGTH_LONG).show();
        }
        return false;

    }

    private void verifyCode(String code) {
        AuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
        reAuthenticateUser(credential);
    }

    private void reAuthenticateUser(AuthCredential credential) {
        currentUser.reauthenticate(credential).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    DeleteMyAccount();
                }
                else {
                    progressBar.setVisibility(View.GONE);
                    AllInputsEnabled(true);
                    Toast.makeText(SettingsActivity.this, task.getException().getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void sendVerificationCode(String number) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                number,
                60,
                TimeUnit.SECONDS,
                TaskExecutors.MAIN_THREAD,
                mCallBack
        );

    }

    private PhoneAuthProvider.OnVerificationStateChangedCallbacks
            mCallBack = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

        @Override
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(s, forceResendingToken);
            verificationId = s;
        }

        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
            String code = phoneAuthCredential.getSmsCode();
            if (code != null) {
                verifyCode(code);
            }
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            progressBar.setVisibility(View.GONE);
            AllInputsEnabled(true);
            Toast.makeText(SettingsActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
        }
    };

}
