package com.example.hp.navigocaptain;

public class RecentHistory {

    private String Name;
    private String Photo;
    private String Time;
    private String Message;
    private String Status;
    private float Rating;

    public RecentHistory() {
    }

    public RecentHistory(String Name, String Photo, String Time, String Message, String Status, float Rating) {
        this.Name = Name;
        this.Photo = Photo;
        this.Time = Time;
        this.Message = Message;
        this.Status = Status;
        this.Rating = Rating;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        this.Name = name;
    }

    public String getPhoto() {
        return Photo;
    }

    public void setPhoto(String photo) {
        this.Photo = photo;
    }

    public String getTime() {
        return Time;
    }

    public void setTime(String time) {
        this.Time = time;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        this.Message = message;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        this.Status = status;
    }

    public float getRating() {
        return Rating;
    }

    public void setRating(float rating) {
        this.Rating = rating;
    }
}
