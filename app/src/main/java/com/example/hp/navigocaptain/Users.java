package com.example.hp.navigocaptain;

import com.google.firebase.database.PropertyName;

import java.io.Serializable;

public class Users implements Serializable {
    private String name, desc, phone, serviceType, photo;

    public Users() {
    }

    public Users(String Name, String Desc, String Phone, String ServiceType, String Photo) {
        name = Name;
        desc = Desc;
        phone = Phone;
        serviceType = ServiceType;
        photo = Photo;
    }

    @PropertyName("Name")
    public String getName() {
        return name;
    }

    @PropertyName("Name")
    public void setName(String Name) {
        name = Name;
    }

    @PropertyName("Desc")
    public String getDesc() {
        return desc;
    }

    @PropertyName("Desc")
    public void setDesc(String Desc) {
        desc = Desc;
    }

    @PropertyName("Phone")
    public String getPhone() {
        return phone;
    }

    @PropertyName("Phone")
    public void setPhone(String Phone) {
        phone = Phone;
    }

    @PropertyName("ServiceType")
    public String getServiceType() {
        return serviceType;
    }

    @PropertyName("ServiceType")
    public void setServiceType(String ServiceType) {
        serviceType = ServiceType;
    }

    @PropertyName("Photo")
    public String getPhoto() {
        return photo;
    }

    @PropertyName("Photo")
    public void setPhoto(String Photo) {
        photo = Photo;
    }
}
