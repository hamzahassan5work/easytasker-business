package com.example.hp.navigocaptain;

import android.Manifest;
import android.app.ActivityOptions;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthRecentLoginRequiredException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import de.hdodenhof.circleimageview.CircleImageView;
import id.zelory.compressor.Compressor;

public class EditProfileActivity extends AppCompatActivity implements OnMapReadyCallback {

    private FirebaseAuth auth;
    private FirebaseUser currentUser;
    private DatabaseReference mdbRef;
    private StorageReference mStorageRef;

    private ProgressBar progressBar;
    private TextView userName, userVotes;
    private EditText userDesc, userPhone, userType;
    private CircleImageView imageView;
    private RadioGroup statusGroup;
    private RatingBar ratingBar;
    private String serviceType;
    private ImageView backbtn, editDesc;

    private Uri imageUri;
    private String profileImageURL ;
    private final static int Request_Code = 007;
    private Bitmap compressedBitmap;
    private byte[] compressByte;

    private ProgressDialog progressDialog;
    private AlertDialog.Builder deleteAccount;
    private String verificationId;

    private static final String TAG = "MapFragment";
    private static final String FINE_LOCATION = android.Manifest.permission.ACCESS_FINE_LOCATION;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1234;

    private boolean mLocationPermissionGranted = false;
    private GoogleMap mMap;
    private boolean Map_type_Normal = true;

    private FusedLocationProviderClient mfusedLocationProviderClient;

    private double Latitude;
    private double Longitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        imageView = (CircleImageView) findViewById(R.id.userProfilePic);
        userName = (TextView) findViewById(R.id.userProfileName);
        userDesc = (EditText) findViewById(R.id.userProfileDesc);
        userPhone = (EditText) findViewById(R.id.userProfilePhone);
        userType = (EditText) findViewById(R.id.servicetype);
        backbtn = (ImageView) findViewById(R.id.backbtn);
        editDesc = (ImageView) findViewById(R.id.editDesc);
        statusGroup = (RadioGroup) findViewById(R.id.statusGroup);
        userVotes = (TextView) findViewById(R.id.totalvotes);
        ratingBar = (RatingBar) findViewById(R.id.ratingBar);

        auth = FirebaseAuth.getInstance();
        currentUser = auth.getCurrentUser();
        mdbRef = FirebaseDatabase.getInstance().getReference();
        mStorageRef = FirebaseStorage.getInstance().getReference();

        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(EditProfileActivity.this, HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });

        userDesc.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
                }
                else {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(),0);
                }
            }
        });

        mdbRef.child("Users").child(currentUser.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()) {
                    Picasso.get()
                            .load(dataSnapshot.child("Photo").getValue().toString())
                            .placeholder(R.drawable.user)
                            .error(R.drawable.user)
                            .into(imageView);

                    userName.setText(dataSnapshot.child("Name").getValue().toString());
                    userDesc.setText(dataSnapshot.child("Desc").getValue().toString());
                    userPhone.setText(dataSnapshot.child("Phone").getValue().toString());
                    userType.setText(dataSnapshot.child("ServiceType").getValue().toString());
                    serviceType = dataSnapshot.child("ServiceType").getValue().toString();

                    if(dataSnapshot.child("Rating").exists()) {
                        ratingBar.setRating(Float.parseFloat(dataSnapshot.child("Rating").getValue().toString()));
                    }
                    if (dataSnapshot.child("Votes").exists()) {
                        userVotes.setText(" " + dataSnapshot.child("Votes").getValue().toString());
                    }

                    if (dataSnapshot.child("TasksLeft").exists()) {
                        int tasksleft = Integer.parseInt(dataSnapshot.child("TasksLeft").getValue().toString());
                        if (tasksleft <= 0) {
                            RadioButton radioAvailable = (RadioButton) findViewById(R.id.radioAvailable);
                            RadioButton radioAway = (RadioButton) findViewById(R.id.radioAway);
                            RadioButton radioClosed = (RadioButton) findViewById(R.id.radioClosed);
                            radioClosed.setChecked(true);
                            statusGroup.setEnabled(false);
                            radioAvailable.setEnabled(false); radioAway.setEnabled(false);
                            return;
                        }

                    }

                    if(dataSnapshot.child("Status").exists()) {
                        String providerStatus = dataSnapshot.child("Status").getValue().toString();
                        RadioButton radioAvailable = (RadioButton) findViewById(R.id.radioAvailable);
                        RadioButton radioAway = (RadioButton) findViewById(R.id.radioAway);
                        RadioButton radioClosed = (RadioButton) findViewById(R.id.radioClosed);
                        if(providerStatus.equals("Available")) {
                            radioAvailable.setChecked(true);
                        }
                        else if(providerStatus.equals("Away")) {
                            radioAway.setChecked(true);
                        }
                        else if(providerStatus.equals("Closed")) {
                            radioClosed.setChecked(true);
                        }
                    }

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        statusGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                RadioButton radioButton = (RadioButton) findViewById(i);
                mdbRef.child("Users").child(currentUser.getUid()).child("Status").setValue(radioButton.getText());
            }
        });

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                PopupMenu popupMenu = new PopupMenu(EditProfileActivity.this, v);
                popupMenu.inflate(R.menu.userphotooptions);
                popupMenu.show();
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        if (menuItem.getItemId() == R.id.changeImage) {
                            selectImage();
                        }
                        else if (menuItem.getItemId() == R.id.viewImage) {
                            Intent intent = new Intent(EditProfileActivity.this, UserImageViewActivity.class);
                            ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(EditProfileActivity.this,
                                    Pair.create(v,"ImageTransition"));
                            startActivity(intent, options.toBundle());
                        }
                        return false;
                    }
                });
            }
        });

        progressDialog = new ProgressDialog(EditProfileActivity.this);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setTitle("Please Wait..");

        deleteAccount = new AlertDialog.Builder(EditProfileActivity.this);
        deleteAccount.setCancelable(false);
        deleteAccount.setTitle("Delete Account");
        deleteAccount.setMessage("Are you sure you want to do this?");
        deleteAccount.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                progressDialog.setMessage("Deleting your Account...");
                progressDialog.show();
                DeleteMyAccount();
            }
        });
        deleteAccount.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });

        getLocationPermission();

    }

    private void selectImage() {
        CropImage.activity().setGuidelines(CropImageView.Guidelines.ON)
                .setAspectRatio(1,1).start( this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                imageUri = result.getUri();
                File file = new File(imageUri.getPath());
                try {
                    compressedBitmap = new Compressor(EditProfileActivity.this).compressToBitmap(file);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                compressedBitmap.compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream);
                compressByte = byteArrayOutputStream.toByteArray();
                uploadImagetoStorage();
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

    private void uploadImagetoStorage() {
        progressBar.setVisibility(View.VISIBLE);
        mStorageRef.child("UserProfilePics").child(currentUser.getUid()).putBytes(compressByte).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                progressBar.setVisibility(View.GONE);
                if(task.isSuccessful()) {
                    imageView.setImageURI(imageUri);
                    task.getResult().getStorage().getDownloadUrl().addOnCompleteListener(new OnCompleteListener<Uri>() {
                        @Override
                        public void onComplete(@NonNull Task<Uri> task) {
                            profileImageURL = task.getResult().toString();
                            mdbRef.child("Users").child(currentUser.getUid()).child("Photo").setValue(profileImageURL);
                            mdbRef.child("Providers").child(serviceType).child(currentUser.getUid()).child("Photo").setValue(profileImageURL);
                            updatePhoto();
                        }
                    });
                }
                else {
                    Toast.makeText(EditProfileActivity.this,task.getException().getMessage(),Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void updatePhoto() {
        UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                .setPhotoUri(Uri.parse(profileImageURL)).build();

        currentUser.updateProfile(profileUpdates).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(Task<Void> task) {
                if (!task.isSuccessful()) {
                    Toast.makeText(EditProfileActivity.this, task.getException().getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void editMyProfile(View view)
    {
        if(editDesc.getTag().equals("editdesc"))
        {
            userDesc.setEnabled(true); userDesc.selectAll(); userDesc.requestFocus();
            editDesc.setImageResource(R.drawable.ic_done_desc_20dp);
            editDesc.setTag("savedesc");

        }
        else {
            if(TextUtils.isEmpty(userDesc.getText())) {
                userDesc.setError("Provide Description"); userDesc.requestFocus();
                return;
            }

            userDesc.setEnabled(false); editDesc.setTag("editdesc"); editDesc.setImageResource(R.drawable.ic_edit_desc_24dp);

            mdbRef.child("Users").child(currentUser.getUid()).child("Desc").setValue(userDesc.getText().toString());
        }
    }

    public void delAccount(View view)
    {
        mdbRef.child("Users").child(currentUser.getUid()).child("CurrentlyServing").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (!dataSnapshot.exists()){
                    deleteAccount.create().show();
                }
                else {
                    Toast.makeText(EditProfileActivity.this,"First Finish Your Ongoing Tasks", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void DeleteMyAccount() {
        mdbRef.child("Types").child(currentUser.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    String type = dataSnapshot.child("ServiceType").getValue().toString();
                    mdbRef.child("Providers").child(type).child(currentUser.getUid()).removeValue();
                    //mdbRef.child("Users").child(currentUser.getUid()).removeValue();
                    mdbRef.child("Types").child(currentUser.getUid()).removeValue();
                    mdbRef.child("RealTimePosition").child(currentUser.getUid()).removeValue();
                    //mStorageRef.child("UserProfilePics").child(currentUser.getUid()).delete();

                    String pushkey = mdbRef.push().getKey();
                    mdbRef.child("DeletedProvidersAccounts").child(pushkey).child("Phone").setValue(currentUser.getPhoneNumber());
                    mdbRef.child("DeletedProvidersAccounts").child(pushkey).child("UID").setValue(currentUser.getUid());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        currentUser.delete().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Intent intentSettings = new Intent(EditProfileActivity.this, PhoneActivity.class);
                    intentSettings.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intentSettings);
                    finish();
                    Toast.makeText(EditProfileActivity.this,"Account Deleted",Toast.LENGTH_LONG).show();
                }
                else if (task.getException() instanceof FirebaseAuthRecentLoginRequiredException) {
                    sendVerificationCode(currentUser.getPhoneNumber());
                }
                else {
                    Toast.makeText(EditProfileActivity.this,task.getException().getMessage(),Toast.LENGTH_LONG).show();
                    progressDialog.cancel();
                }
            }
        });
    }

    private void verifyCode(String code) {
        AuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
        reAuthenticateUser(credential);
    }

    private void reAuthenticateUser(AuthCredential credential) {
        currentUser.reauthenticate(credential).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    DeleteMyAccount();
                }
                else {
                    progressDialog.cancel();
                    Toast.makeText(EditProfileActivity.this, task.getException().getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void sendVerificationCode(String number) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                number,
                60,
                TimeUnit.SECONDS,
                TaskExecutors.MAIN_THREAD,
                mCallBack
        );

    }

    private PhoneAuthProvider.OnVerificationStateChangedCallbacks
            mCallBack = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

        @Override
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(s, forceResendingToken);
            verificationId = s;
        }

        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
            String code = phoneAuthCredential.getSmsCode();
            if (code != null) {
                verifyCode(code);
            }
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            progressDialog.cancel();
            Toast.makeText(EditProfileActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
        }
    };

    public void showHistory(View view){
        startActivity(new Intent(EditProfileActivity.this, HistoryActivity.class));
    }

    private void initMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(EditProfileActivity.this);
    }

    private void getLocationPermission() {
        String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION};

        if (ContextCompat.checkSelfPermission(EditProfileActivity.this,FINE_LOCATION)  == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;
            initMap();
        }
        else {
            ActivityCompat.requestPermissions(EditProfileActivity.this,permissions,LOCATION_PERMISSION_REQUEST_CODE);
        }

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        /*try {
            boolean success = googleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            this, R.raw.mapstyle));

            if (!success) {
                Log.e(TAG, "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e(TAG, "Can't find style. Error: ", e);
        }*/

        if (mLocationPermissionGranted) {
            getServiceProviderLocation();
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            mMap.setMyLocationEnabled(false);
            mMap.getUiSettings().setMyLocationButtonEnabled(false);
            mMap.getUiSettings().setIndoorLevelPickerEnabled(false);
            mMap.getUiSettings().setZoomControlsEnabled(false);
            mMap.getUiSettings().setAllGesturesEnabled(false);
            mMap.getUiSettings().setMapToolbarEnabled(true);
        }
    }

    private void getServiceProviderLocation() {
        mdbRef.child("Users").child(currentUser.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.child("Latitude").exists() && dataSnapshot.child("Longitude").exists()) {
                    Latitude = (double) dataSnapshot.child("Latitude").getValue();
                    Longitude = (double) dataSnapshot.child("Longitude").getValue();
                    moveCamera(new LatLng(Latitude, Longitude), 15f);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void moveCamera(LatLng latLng, float v) {
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, v));
        MarkerOptions markerOptions = new MarkerOptions().position(latLng);
        mMap.addMarker(markerOptions);
    }
}
