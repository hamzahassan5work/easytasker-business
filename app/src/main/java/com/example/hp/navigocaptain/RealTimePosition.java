package com.example.hp.navigocaptain;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class RealTimePosition extends Service {

    private FirebaseAuth auth;
    private FirebaseUser currentUser;
    private DatabaseReference mdbRef;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        auth = FirebaseAuth.getInstance();
        currentUser = auth.getCurrentUser();
        mdbRef = FirebaseDatabase.getInstance().getReference();
    }

    @SuppressLint("MissingPermission")
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("BackgroundLocationService", "Monitoring Realtime Location", NotificationManager.IMPORTANCE_DEFAULT);
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);

            PendingIntent pIntent = PendingIntent.getActivity(this, 0, new Intent(this, HomeActivity.class), 0);

            Notification notification = new NotificationCompat.Builder(this, "BackgroundLocationService")
                    .setContentTitle("Easytasker Business")
                    .setContentText("Monitoring RealTime Location..")
                    .setContentIntent(pIntent).build();

            startForeground(1, notification);

        }

        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 0, new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                if (location != null) {
                    mdbRef.child("RealTimePosition").child(currentUser.getUid()).child("Speed").setValue(location.getSpeed());
                    mdbRef.child("RealTimePosition").child(currentUser.getUid()).child("Latitude").setValue(location.getLatitude());
                    mdbRef.child("RealTimePosition").child(currentUser.getUid()).child("Longitude").setValue(location.getLongitude());
                    mdbRef.child("RealTimePosition").child(currentUser.getUid()).child("LastUpdated").setValue(new SimpleDateFormat("dd MMM hh:mm a").format(Calendar.getInstance().getTime()));
                }
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {

            }
        });
        
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Intent broadcastIntent = new Intent(this, LocationServiceBroadCastReciever.class);
        sendBroadcast(broadcastIntent);
    }

}
