package com.example.hp.navigocaptain;

import android.Manifest;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.ApplicationErrorReport;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthRecentLoginRequiredException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.security.Permissions;
import java.security.cert.TrustAnchor;
import java.util.concurrent.TimeUnit;

public class HomeActivity extends AppCompatActivity {

    private FirebaseAuth auth;
    private FirebaseUser currentUser;
    private DatabaseReference mdbRef;
    private StorageReference mStorageRef;

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    private TextView accountBalance;

    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1234;
    private static final int FOREGROUND_SERVICE_REQUEST_CODE = 4321;
    private static final int REQUEST_CALL = 007;

    private String verificationId;
    private Snackbar connectionSnackbar, gpsSnackbar, rechargeAccountSnackbar;
    private boolean showrechargesnackbar = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        View parentLayout = findViewById(R.id.main_content);
        connectionSnackbar = Snackbar.make(parentLayout, "No Internet Connection", Snackbar.LENGTH_INDEFINITE)
                .setAction("CLOSE", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //
                    }
                });
        gpsSnackbar = Snackbar.make(parentLayout,"Turn on your GPS for this app (Recommended)", Snackbar.LENGTH_INDEFINITE)
                .setAction("TURN ON", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intentgps = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(intentgps);
                    }
                });
        rechargeAccountSnackbar = Snackbar.make(parentLayout,"Please recharge your account credit", Snackbar.LENGTH_INDEFINITE)
                .setAction("RECHARGE", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startActivity(new Intent(HomeActivity.this, TaskcreditActivity.class));
                    }
                });

        auth = FirebaseAuth.getInstance();
        currentUser = auth.getCurrentUser();
        mdbRef = FirebaseDatabase.getInstance().getReference();
        mStorageRef = FirebaseStorage.getInstance().getReference();

        // check that provider made his profile or not
        if(currentUser.getDisplayName() == null || currentUser.getPhotoUrl() == null)
        {
            Intent intentSettings = new Intent(HomeActivity.this, SettingsActivity.class);
            intentSettings.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intentSettings);
            finish();
            return;
        }

        if (ActivityCompat.checkSelfPermission(HomeActivity.this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(HomeActivity.this,new String[] {android.Manifest.permission.CALL_PHONE}, REQUEST_CALL);
        }

        String deviceToken = FirebaseInstanceId.getInstance().getToken();
        mdbRef.child("Users").child(currentUser.getUid()).child("Token").setValue(deviceToken);
        RealTimePosition();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));

    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Check Internet Availability
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();

        boolean gpsStatus = CheckGpsStatus();

        if (!isConnected) {
            if (!connectionSnackbar.isShown()) {
                connectionSnackbar.show();
            }
        }
        else {
            if (connectionSnackbar.isShown()) {
                connectionSnackbar.dismiss();
            }
            if (!gpsStatus) {
                if (!gpsSnackbar.isShown()) {
                    gpsSnackbar.show();
                }
            }
            else {
                if (gpsSnackbar.isShown()) {
                    gpsSnackbar.dismiss();
                }
                if (showrechargesnackbar) {
                    rechargeAccountSnackbar.show();
                }
                else {
                    rechargeAccountSnackbar.dismiss();
                }
            }

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.settings, menu);

        final MenuItem taskcredit = menu.findItem(R.id.taskcredit);
        View actionView = taskcredit.getActionView();
        accountBalance = (TextView) actionView.findViewById(R.id.cart_badge);

        mdbRef.child("Users").child(currentUser.getUid()).child("TasksLeft").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    String tasksleft = String.valueOf(dataSnapshot.getValue());
                    accountBalance.setText(tasksleft);
                    if (Integer.parseInt(tasksleft) <= 0) {
                        mdbRef.child("Users").child(currentUser.getUid()).child("Status").setValue("Closed");
                        showrechargesnackbar = true;
                        rechargeAccountSnackbar.show();
                    }
                    else {
                        mdbRef.child("Users").child(currentUser.getUid()).child("Status").setValue("Available");
                        showrechargesnackbar = false;
                        rechargeAccountSnackbar.dismiss();
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        actionView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOptionsItemSelected(taskcredit);
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.editprofile) {
            Intent intent = new Intent(HomeActivity.this, EditProfileActivity.class);
            startActivity(intent);
        }
        else if (item.getItemId() == R.id.history) {
            Intent intent = new Intent(HomeActivity.this, HistoryActivity.class);
            startActivity(intent);
        }
        else if (item.getItemId() == R.id.taskcredit) {
            Intent intent = new Intent(HomeActivity.this, TaskcreditActivity.class);
            startActivity(intent);
        }
        else if (item.getItemId() == R.id.about) {
            startActivity(new Intent(HomeActivity.this, AboutActivity.class));
        }

        return super.onOptionsItemSelected(item);

    }

    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = null;
            switch (getArguments().getInt(ARG_SECTION_NUMBER))
            {
                case 1:
                    rootView = inflater.inflate(R.layout.fragment_requests, container, false);
                    break;
                case 2:
                    rootView = inflater.inflate(R.layout.fragment_ongoing, container, false);
                    break;
            }
            return rootView;
        }
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    RequestsFragment requestsFragment = new RequestsFragment();
                    return requestsFragment;
                case 1:
                    OngoingFragment ongoingFragment = new OngoingFragment();
                    return ongoingFragment;
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            // Show 2 total pages.
            return 2;
        }
    }

    private void RealTimePosition() {
        String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION};
        if (ActivityCompat.checkSelfPermission(HomeActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(HomeActivity.this,permissions,LOCATION_PERMISSION_REQUEST_CODE);
        }
        else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                if (ActivityCompat.checkSelfPermission(HomeActivity.this, android.Manifest.permission.FOREGROUND_SERVICE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(HomeActivity.this, new String[]{Manifest.permission.FOREGROUND_SERVICE}, FOREGROUND_SERVICE_REQUEST_CODE);
                }
                else {
                    if(!isMyServiceRunning(RealTimePosition.class)) {
                        startForegroundService(new Intent(HomeActivity.this, RealTimePosition.class));
                    }
                }
            }
            else {
                if(!isMyServiceRunning(RealTimePosition.class)) {
                    startService(new Intent(HomeActivity.this, RealTimePosition.class));
                }
            }
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case LOCATION_PERMISSION_REQUEST_CODE: {
                if(grantResults.length > 0) {
                    for(int i=0; i < grantResults.length; i++) {
                        if(grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                            return;
                        }
                    }
                    RealTimePosition();
                }
            }
            case REQUEST_CALL: {
                if (grantResults.length <= 0 && grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(HomeActivity.this,"Permission Denied", Toast.LENGTH_LONG).show();
                }
            }
            case FOREGROUND_SERVICE_REQUEST_CODE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.i("ForegroundService: ", "PermissionGranted");
                    RealTimePosition();
                }
            }
        }
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    private boolean CheckGpsStatus(){

        LocationManager locationManagergps = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        boolean GpsStatus = locationManagergps.isProviderEnabled(LocationManager.GPS_PROVIDER);

        if(GpsStatus)
        {
            return true;
        }
        else {
            return false;
        }

    }
}
