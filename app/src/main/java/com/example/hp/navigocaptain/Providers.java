package com.example.hp.navigocaptain;

import com.google.firebase.database.PropertyName;

import java.io.Serializable;

public class Providers implements Serializable {

    private String phone;
    private String name;
    private String photo;

    public Providers() {
    }

    public Providers(String Phone, String Name, String url) {
        phone = Phone;
        name = Name;
        photo = url;
    }

    @PropertyName("Phone")
    public String getPhone() {
        return phone;
    }

    @PropertyName("Phone")
    public void setPhone(String Phone) {
        phone = Phone;
    }

    @PropertyName("Name")
    public String getName() {
        return name;
    }

    @PropertyName("Name")
    public void setName(String Name) {
        name = Name;
    }

    @PropertyName("Photo")
    public String getPhoto() {
        return photo;
    }

    @PropertyName("Photo")
    public void setPhoto(String Photo) {
        photo = Photo;
    }
}
