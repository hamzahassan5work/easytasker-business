package com.example.hp.navigocaptain;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

public class UserImageViewActivity extends AppCompatActivity {
    private FirebaseAuth auth;
    private FirebaseUser currentUser;
    private DatabaseReference mdbRef;

    private ImageView backBtn, userImageView;
    private TextView activityLabel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_image_view);

        auth = FirebaseAuth.getInstance();
        currentUser = auth.getCurrentUser();
        mdbRef = FirebaseDatabase.getInstance().getReference();

        backBtn = findViewById(R.id.backbtn);
        userImageView = findViewById(R.id.userimageView);
        activityLabel = findViewById(R.id.activityLabel);

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(UserImageViewActivity.this, EditProfileActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });

        mdbRef.child("Users").child(currentUser.getUid()).child("Name").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    activityLabel.setText(dataSnapshot.getValue().toString());
                }
                else {
                    activityLabel.setText("N/A");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                activityLabel.setText("N/A");
            }
        });

        Picasso.get()
                .load(currentUser.getPhotoUrl())
                .placeholder(R.drawable.user)
                .error(R.drawable.user)
                .into(userImageView);

    }
}
