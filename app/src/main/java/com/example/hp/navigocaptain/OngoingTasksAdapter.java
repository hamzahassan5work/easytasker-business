package com.example.hp.navigocaptain;

import android.app.Activity;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class OngoingTasksAdapter extends RecyclerView.Adapter<OngoingTasksAdapter.OngoingTasksViewHolder> {

    private FirebaseAuth auth = FirebaseAuth.getInstance();
    private FirebaseUser currentUser = auth.getCurrentUser();
    private DatabaseReference mdbRef = FirebaseDatabase.getInstance().getReference();
    private Context context;
    private List<OngoingTasks> ongoingTasks;

    public OngoingTasksAdapter(Context context, List<OngoingTasks> ongoingTasks) {
        this.context = context;
        this.ongoingTasks = ongoingTasks;
    }

    @NonNull
    @Override
    public OngoingTasksAdapter.OngoingTasksViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.ongoing_tasks,parent,false);
        return new OngoingTasksViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final OngoingTasksViewHolder holder, int i) {

        OngoingTasks mOngoingTasks = ongoingTasks.get(i);
        holder.Name.setText(mOngoingTasks.getName());
        holder.Time.setText(mOngoingTasks.getTime());
        holder.Message.setText(mOngoingTasks.getMessage());
        Picasso.get()
                .load(mOngoingTasks.getPhoto())
                .placeholder(R.drawable.user)
                .error(R.drawable.user)
                .into(holder.Image);

        final double latitude = mOngoingTasks.getLatitude();
        final double longitude = mOngoingTasks.getLongitude();
        final String ckey = mOngoingTasks.getCkey();

        mdbRef.child("RealTimePosition").child(currentUser.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.child("Latitude").exists() && dataSnapshot.child("Longitude").exists()) {
                    double plat = (double) dataSnapshot.child("Latitude").getValue();
                    double plng = (double) dataSnapshot.child("Longitude").getValue();
                    // Calculating distance between customer and provider
                    Location startPoint = new Location("Provider");
                    startPoint.setLatitude(plat);
                    startPoint.setLongitude(plng);

                    Location endPoint = new Location("Customer");
                    endPoint.setLatitude(latitude);
                    endPoint.setLongitude(longitude);

                    DecimalFormat decimalFormat = new DecimalFormat("#0.00");
                    double distance = startPoint.distanceTo(endPoint);
                    String distanceInUnit = decimalFormat.format(distance/1000) + " km";
                    holder.custDirection.setText(distanceInUnit);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        holder.finishWork.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mdbRef.child("AcceptedOrders").child(currentUser.getUid()).child(ckey).removeValue();
                mdbRef.child("Users").child(currentUser.getUid()).child("CurrentlyServing").child(ckey).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if(dataSnapshot.exists()) {
                            final String pushKey = String.valueOf(dataSnapshot.child("PushKey").getValue());
                            final String HistoryKey = String.valueOf(dataSnapshot.child("HistoryKey").getValue());
                            mdbRef.child("ActiveServices").child(ckey).child(pushKey).child("Status").setValue("Finished");
                            mdbRef.child("Users").child(currentUser.getUid()).child("History").child(HistoryKey).addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    String time = dataSnapshot.child("Time").getValue().toString();
                                    mdbRef.child("ActiveServices").child(ckey).child(pushKey).child("Time").setValue(time + " - " + new SimpleDateFormat("dd MMM hh:mm a").format(Calendar.getInstance().getTime()));
                                    mdbRef.child("Users").child(currentUser.getUid()).child("History").child(HistoryKey).child("Time").setValue(time + " - " + new SimpleDateFormat("dd MMM hh:mm a").format(Calendar.getInstance().getTime()));
                                    mdbRef.child("Users").child(currentUser.getUid()).child("History").child(HistoryKey).child("Status").setValue("Finished");
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });
                            mdbRef.child("Users").child(currentUser.getUid()).child("CurrentlyServing").child(ckey).removeValue();
                            // For Rating Notifications
                            HashMap<String,Object> ratingAssests = new HashMap<>();
                            ratingAssests.put("Id", currentUser.getUid());
                            ratingAssests.put("Name",currentUser.getDisplayName());
                            ratingAssests.put("ProviderHistoryKey", HistoryKey);
                            mdbRef.child("PendingRatings").child(ckey).push().setValue(ratingAssests);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

            }
        });

        holder.custDirection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, GetDirection.class);
                intent.putExtra("Latitude", latitude);
                intent.putExtra("Longitude", longitude);
                context.startActivity(intent);
            }
        });

        holder.optionsMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(context,holder.optionsMenu);
                popupMenu.inflate(R.menu.cardviewoptions);
                popupMenu.show();
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        switch (menuItem.getItemId()){
                            case R.id.deltask:
                                mdbRef.child("AcceptedOrders").child(currentUser.getUid()).child(ckey).removeValue();
                                mdbRef.child("Users").child(currentUser.getUid()).child("CurrentlyServing").child(ckey).addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        if(dataSnapshot.exists()) {
                                            final String pushKey = String.valueOf(dataSnapshot.child("PushKey").getValue());
                                            final String historyKey = String.valueOf(dataSnapshot.child("HistoryKey").getValue());
                                            mdbRef.child("ActiveServices").child(ckey).child(pushKey).child("Status").setValue("Cancelled");
                                            mdbRef.child("Users").child(currentUser.getUid()).child("History").child(historyKey).addListenerForSingleValueEvent(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                    String time = dataSnapshot.child("Time").getValue().toString();
                                                    mdbRef.child("ActiveServices").child(ckey).child(pushKey).child("Time").setValue(time + " - " + new SimpleDateFormat("dd MMM hh:mm a").format(Calendar.getInstance().getTime()));
                                                    mdbRef.child("Users").child(currentUser.getUid()).child("History").child(historyKey).child("Time").setValue(time + " - " + new SimpleDateFormat("dd MMM hh:mm a").format(Calendar.getInstance().getTime()));
                                                    mdbRef.child("Users").child(currentUser.getUid()).child("History").child(historyKey).child("Status").setValue("Cancelled");
                                                }

                                                @Override
                                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                                }
                                            });
                                            mdbRef.child("Users").child(currentUser.getUid()).child("CurrentlyServing").child(ckey).removeValue();
                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });
                                break;
                            case R.id.call:
                                if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                    Toast.makeText(context,"Permission Denied. Restart Application", Toast.LENGTH_LONG).show();
                                }
                                else {
                                    mdbRef.child("Users").child(ckey).addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                            if (dataSnapshot.child("Phone").exists()) {
                                                String phonenum = dataSnapshot.child("Phone").getValue().toString();
                                                context.startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phonenum)));
                                            }
                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError databaseError) {

                                        }
                                    });

                                }
                                break;
                            default:
                                break;
                        }
                        return false;
                    }
                });
            }
        });

    }

    @Override
    public int getItemCount() {
        return ongoingTasks.size();
    }

    public class OngoingTasksViewHolder extends RecyclerView.ViewHolder{

        public CardView cardView;
        public CircleImageView Image;
        public ImageView optionsMenu;
        public TextView Name;
        public TextView Time;
        public TextView Message;
        public Button finishWork, custDirection;

        public OngoingTasksViewHolder(@NonNull View itemView) {
            super(itemView);

            cardView = (CardView) itemView.findViewById(R.id.customerRequestAccepted);
            Image = (CircleImageView) itemView.findViewById(R.id.customerPic);
            optionsMenu = (ImageView) itemView.findViewById(R.id.optionsMenu);
            Name = (TextView) itemView.findViewById(R.id.customerName);
            Time = (TextView) itemView.findViewById(R.id.time);
            Message = (TextView) itemView.findViewById(R.id.custMsg);
            finishWork = (Button) itemView.findViewById(R.id.finishCust);
            custDirection = (Button) itemView.findViewById(R.id.getDirection);

        }
    }

}
