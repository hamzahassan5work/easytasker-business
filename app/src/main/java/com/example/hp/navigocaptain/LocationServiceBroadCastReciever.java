package com.example.hp.navigocaptain;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

public class LocationServiceBroadCastReciever extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            context.startForegroundService(new Intent(context, RealTimePosition.class));
        }
        else {
            context.startService(new Intent(context, RealTimePosition.class));
        }
    }
}
