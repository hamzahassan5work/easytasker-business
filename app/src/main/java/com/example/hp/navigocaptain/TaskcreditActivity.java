package com.example.hp.navigocaptain;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.Api;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

public class TaskcreditActivity extends AppCompatActivity {

    private FirebaseAuth auth;
    private FirebaseUser currentUser;
    private DatabaseReference mdbRef;

    private RelativeLayout verificationProcess, paymentsPanel;
    private ImageView backBtn, accountcreditInfo;
    private EditText EasypaisaID, totalAmount;
    private TextView note, quantity, accountBalance, requestStatus;
    private Button addBtn, removeBtn, submit;
    private ProgressBar progressBar;
    private AlertDialog.Builder submitdialog, creditInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_taskcredit);

        auth = FirebaseAuth.getInstance();
        currentUser = auth.getCurrentUser();
        mdbRef = FirebaseDatabase.getInstance().getReference();

        verificationProcess = (RelativeLayout) findViewById(R.id.VerificationProcess);
        paymentsPanel = (RelativeLayout) findViewById(R.id.Paymentspanel);
        backBtn = (ImageView) findViewById(R.id.backbtn);
        EasypaisaID = (EditText) findViewById(R.id.accountid);
        totalAmount = (EditText) findViewById(R.id.totalamount);
        quantity = (TextView) findViewById(R.id.quantity);
        note = (TextView) findViewById(R.id.note);
        accountcreditInfo = (ImageView) findViewById(R.id.accountinfo);
        accountBalance = (TextView) findViewById(R.id.accountcreditbalance);
        requestStatus = (TextView) findViewById(R.id.requestStatus);
        addBtn = (Button) findViewById(R.id.addquantity);
        removeBtn = (Button) findViewById(R.id.removequantity);
        submit = (Button) findViewById(R.id.submit);
        progressBar = (ProgressBar) findViewById(R.id.mprogressBar);

        mdbRef.child("CreditBuyRequest").child(currentUser.getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    requestStatus.setText("Your request against " + dataSnapshot.child("Credit").getValue().toString() + " Credits is in a verification process, we will notify you via SMS once its done.");
                    verificationProcess.setVisibility(View.VISIBLE); paymentsPanel.setVisibility(View.GONE);
                }
                else {
                    requestStatus.setText("");
                    verificationProcess.setVisibility(View.GONE); paymentsPanel.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        EasypaisaID.setText(currentUser.getPhoneNumber());

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentSettings = new Intent(TaskcreditActivity.this, HomeActivity.class);
                intentSettings.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentSettings);
                finish();
            }
        });

        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int quantitynum = Integer.parseInt(quantity.getText().toString());
                if (quantitynum < 30) {
                    quantity.setText(String.valueOf(quantitynum + 1));
                    totalAmount.setText(String.valueOf((quantitynum + 1) * 30));
                    note.setText("Note: Proceed this form only once you transfer the funds (i-e Rs: "+ (quantitynum + 1) * 30 +"/-) from above mentioned account ID to 03067396990 via Easypaisa.");
                }
            }
        });

        removeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int quantitynum = Integer.parseInt(quantity.getText().toString());
                if (quantitynum > 1) {
                    quantity.setText(String.valueOf(quantitynum - 1));
                    totalAmount.setText(String.valueOf((quantitynum - 1) * 30));
                    note.setText("Note: Proceed this form only once you transfer the funds (i-e Rs: "+ (quantitynum - 1) * 30 +"/-) from above mentioned account ID to 03067396990 via Easypaisa.");
                }
            }
        });

        submitdialog = new AlertDialog.Builder(TaskcreditActivity.this);
        submitdialog.setCancelable(false);
        submitdialog.setTitle("Payments");

        creditInfo = new AlertDialog.Builder(TaskcreditActivity.this);
        creditInfo.setTitle("About Account Credit");
        creditInfo.setMessage("The account credit displays in numbers which represents how many times you can accept task requests from customers means 1 credit equals to 1 accepted request. " +
                "When current account credit becomes zero your service status will set to closed until you add credits to your account through buying them via Easypaisa.");
        creditInfo.setPositiveButton("GOT IT", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });

        accountcreditInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                creditInfo.create().show();
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitdialog.setMessage("Have you transferred the Rs: " + totalAmount.getText() +"/- from ID " + EasypaisaID.getText() +" to 03067396990?");
                submitdialog.create().show();
            }
        });

        submitdialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                SavePaymentstoDB();
            }
        });
        submitdialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });

        mdbRef.child("Users").child(currentUser.getUid()).child("TasksLeft").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    accountBalance.setText("Current Account Credit: " + dataSnapshot.getValue().toString());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void SavePaymentstoDB() {
        //progressBar.setVisibility(View.VISIBLE);
        submit.setEnabled(false);
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("Name", currentUser.getDisplayName());
        hashMap.put("UID", currentUser.getUid());
        hashMap.put("Time", new SimpleDateFormat("dd MMM hh:mm a").format(Calendar.getInstance().getTime()));
        hashMap.put("EasypaisaID", EasypaisaID.getText().toString());
        hashMap.put("Credit", quantity.getText().toString());
        hashMap.put("Amount", totalAmount.getText().toString());
        hashMap.put("Status", "InProcess");

        mdbRef.child("CreditBuyRequest").child(currentUser.getUid()).setValue(hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                //progressBar.setVisibility(View.GONE);
                submit.setEnabled(true);
                if (task.isSuccessful()) {
                    Toast.makeText(TaskcreditActivity.this,"Submitted Successfully",Toast.LENGTH_LONG).show();
                    requestStatus.setText("Your request against " + quantity.getText().toString() + " Credits is in a verification process, we will notify you via SMS once its done.");
                    paymentsPanel.setVisibility(View.GONE); verificationProcess.setVisibility(View.VISIBLE);
                    quantity.setText("1"); totalAmount.setText("30");
                }
                else {
                    Toast.makeText(TaskcreditActivity.this,task.getException().getMessage(),Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}
