package com.example.hp.navigocaptain;

public class OngoingTasks {

    private String Name;
    private String Photo;
    private String Time;
    private String Message;
    private String Ckey;
    private double Latitude;
    private double Longitude;

    public OngoingTasks() {
    }

    public OngoingTasks(String Name, String Photo, String Time, String Message, String Ckey, double Latitude, double Longitude) {
        this.Name = Name;
        this.Photo = Photo;
        this.Time = Time;
        this.Message = Message;
        this.Ckey = Ckey;
        this.Latitude = Latitude;
        this.Longitude = Longitude;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        this.Name = name;
    }

    public String getPhoto() {
        return Photo;
    }

    public void setPhoto(String photo) {
        this.Photo = photo;
    }

    public String getTime() {
        return Time;
    }

    public void setTime(String time) {
        this.Time = time;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        this.Message = message;
    }

    public String getCkey() {
        return Ckey;
    }

    public void setCkey(String ckey) {
        this.Ckey = ckey;
    }

    public double getLatitude() {
        return Latitude;
    }

    public void setLatitude(double latitude) {
        this.Latitude = latitude;
    }

    public double getLongitude() {
        return Longitude;
    }

    public void setLongitude(double longitude) {
        this.Longitude = longitude;
    }
}
