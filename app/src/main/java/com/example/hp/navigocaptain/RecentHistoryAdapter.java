package com.example.hp.navigocaptain;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.jar.Attributes;

import de.hdodenhof.circleimageview.CircleImageView;

public class RecentHistoryAdapter extends RecyclerView.Adapter<RecentHistoryAdapter.RecentHistoryViewHolder> {

    private FirebaseAuth auth = FirebaseAuth.getInstance();
    private FirebaseUser currentUser = auth.getCurrentUser();
    private DatabaseReference mdbRef = FirebaseDatabase.getInstance().getReference();
    private Context context;
    private List<RecentHistory> recentHistory;

    public RecentHistoryAdapter(Context context, List<RecentHistory> recentHistory) {
        this.context = context;
        this.recentHistory = recentHistory;
    }

    @NonNull
    @Override
    public RecentHistoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.recenthistory,parent,false);
        return new RecentHistoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecentHistoryViewHolder holder, int i) {
        final RecentHistory mRecentHistory = recentHistory.get(i);
        holder.Name.setText(mRecentHistory.getName());
        holder.Time.setText(mRecentHistory.getTime());
        if (mRecentHistory.getStatus().equals("Finished")) {
            holder.Status.setTextColor(Color.parseColor("#00574B"));
        } else if (mRecentHistory.getStatus().equals("Cancelled")) {
            holder.Status.setTextColor(Color.parseColor("#6c6c6c"));
        } else {
            holder.Status.setTextColor(Color.parseColor("#D12B60"));
        }
        holder.Status.setText(mRecentHistory.getStatus());
        holder.Message.setText(mRecentHistory.getMessage());
        holder.ratingBar.setRating(mRecentHistory.getRating());

        String cUID = mRecentHistory.getPhoto();
        mdbRef.child("Users").child(cUID).child("Photo").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    Picasso.get()
                            .load(dataSnapshot.getValue().toString())
                            .placeholder(R.drawable.user)
                            .error(R.drawable.user)
                            .into(holder.Image);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return recentHistory.size();
    }

    public class RecentHistoryViewHolder extends RecyclerView.ViewHolder {

        public CircleImageView Image;
        public TextView Name;
        public TextView Time;
        public TextView Status;
        public TextView Message;
        public RatingBar ratingBar;

        public RecentHistoryViewHolder(@NonNull View itemView) {
            super(itemView);

            Image = (CircleImageView) itemView.findViewById(R.id.customerPic);
            Name = (TextView) itemView.findViewById(R.id.customerName);
            Time = (TextView) itemView.findViewById(R.id.time);
            Status = (TextView) itemView.findViewById(R.id.taskstatus);
            Message = (TextView) itemView.findViewById(R.id.custMsg);
            ratingBar = (RatingBar) itemView.findViewById(R.id.ratingBar);

        }
    }
}
