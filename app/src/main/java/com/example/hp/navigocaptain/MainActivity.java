package com.example.hp.navigocaptain;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CursorTreeAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.squareup.picasso.Picasso;

import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity {

    private FirebaseAuth auth;
    private FirebaseUser currentUser;
    private DatabaseReference mdbRef;

    private CardView cardView;
    private CircleImageView imageView;
    private TextView custName;
    private TextView time, custMessage;
    private Button acceptCustomer;
    private Button rejectCustomer;
    private Button getDirection;
    private Button finishcust;

    private double Latitude;
    private double Longitude;
    private String cusPhotoURL;
    private ProgressDialog confirmCustomer;

    private String ckey;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        auth = FirebaseAuth.getInstance();
        currentUser = auth.getCurrentUser();
        mdbRef = FirebaseDatabase.getInstance().getReference();

        // check that provider made his profile or not
        if(currentUser.getDisplayName() == null || currentUser.getPhotoUrl() == null)
        {
            Intent intentSettings = new Intent(MainActivity.this, SettingsActivity.class);
            intentSettings.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intentSettings);
            finish();
        }

        String deviceToken = FirebaseInstanceId.getInstance().getToken();
        mdbRef.child("Users").child(currentUser.getUid()).child("Token").setValue(deviceToken);

        cardView = (CardView) findViewById(R.id.customerRequest);
        imageView = (CircleImageView) findViewById(R.id.customerPic);
        custName = (TextView) findViewById(R.id.customerName);
        custMessage = (TextView) findViewById(R.id.custMsg);
        time = (TextView) findViewById(R.id.time);
        getDirection = (Button) findViewById(R.id.getDirection);
        acceptCustomer = (Button) findViewById(R.id.acceptCust);
        rejectCustomer = (Button) findViewById(R.id.rejectCust);
        finishcust = (Button) findViewById(R.id.finishCust);

        confirmCustomer = new ProgressDialog(this);
        confirmCustomer.setTitle("Confirming..");
        confirmCustomer.setCanceledOnTouchOutside(false);

        mdbRef.child("Orders").child(currentUser.getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    cardView.setVisibility(View.VISIBLE);
                    for(DataSnapshot singleOrder : dataSnapshot.getChildren()) {
                        custName.setText(singleOrder.child("Name").getValue().toString());
                        confirmCustomer.setMessage("Please wait until the customer confirms the service ...");
                        time.setText(singleOrder.child("Time").getValue().toString());
                        custMessage.setText(singleOrder.child("Message").getValue().toString());
                        ckey = singleOrder.getKey();

                        String status = singleOrder.child("Status").getValue().toString();
                        if(status.equals("2")){
                            confirmCustomer.dismiss();
                            acceptCustomer.setVisibility(View.GONE);
                            rejectCustomer.setVisibility(View.GONE);
                            getDirection.setVisibility(View.VISIBLE);
                            finishcust.setVisibility(View.VISIBLE);
                            Toast.makeText(MainActivity.this,"Customer just confirmed the service",Toast.LENGTH_LONG).show();
                            mdbRef.child("Users").child(currentUser.getUid()).child("CurrentlyServing").child(ckey).child("Status").setValue("Ongoing");
                        }
                        else if(status.equals("-2")){
                            confirmCustomer.dismiss();
                            Toast.makeText(MainActivity.this,"Customer just dismissed the service",Toast.LENGTH_LONG).show();
                            mdbRef.child("Orders").child(currentUser.getUid()).removeValue();
                            cardView.setVisibility(View.GONE);
                        }


                        mdbRef.child("Users").child(ckey).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                cusPhotoURL = dataSnapshot.child("Photo").getValue().toString();
                                Picasso.get()
                                        .load(cusPhotoURL)
                                        .error(R.drawable.userprofilepic)
                                        .into(imageView);
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });

                        Latitude = (Double) singleOrder.child("Latitude").getValue();
                        Longitude = (Double) singleOrder.child("Longitude").getValue();
                    }
                }
                else {
                    cardView.setVisibility(View.GONE);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        rejectCustomer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cardView.setVisibility(View.GONE);
                mdbRef.child("Orders").child(currentUser.getUid()).child(ckey).child("Status").setValue("-1");
            }
        });

        acceptCustomer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!(MainActivity.this).isFinishing())
                {
                    confirmCustomer.show();
                }
                mdbRef.child("Orders").child(currentUser.getUid()).child(ckey).child("Status").setValue("1");
            }
        });

        getDirection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, GetDirection.class);
                intent.putExtra("Latitude", Latitude);
                intent.putExtra("Longitude", Longitude);
                startActivity(intent);
            }
        });

        finishcust.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mdbRef.child("Orders").child(currentUser.getUid()).child(ckey).removeValue();
                mdbRef.child("Users").child(currentUser.getUid()).child("CurrentlyServing").child(ckey).removeValue();
                cardView.setVisibility(View.GONE);

                // For Rating Notifications
                HashMap<String,Object> ratingAssests = new HashMap<>();
                ratingAssests.put("Id", currentUser.getUid());
                ratingAssests.put("Name",currentUser.getDisplayName());
                mdbRef.child("PendingRatings").child(ckey).push().setValue(ratingAssests);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.settings, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.editprofile) {
            Intent intent = new Intent(MainActivity.this, EditProfileActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }


}
