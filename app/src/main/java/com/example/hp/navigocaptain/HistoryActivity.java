package com.example.hp.navigocaptain;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class HistoryActivity extends AppCompatActivity {

    private FirebaseAuth auth;
    private FirebaseUser currentUser;
    private DatabaseReference mdbRef;

    private ImageView icon;
    private TextView text;

    private RecyclerView recyclerView;
    private List<RecentHistory> recentHistories;
    private RecentHistoryAdapter recentHistoryAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        auth = FirebaseAuth.getInstance();
        currentUser = auth.getCurrentUser();
        mdbRef = FirebaseDatabase.getInstance().getReference();

        recyclerView = (RecyclerView) findViewById(R.id.RecyclerView);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setReverseLayout(true);
        layoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(layoutManager);
        recentHistories = new ArrayList<>();

        icon = (ImageView) findViewById(R.id.icon);
        text = (TextView) findViewById(R.id.Activetext);

        mdbRef.child("Users").child(currentUser.getUid()).child("History").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    recentHistories.clear();
                    recyclerView.setVisibility(View.VISIBLE);
                    icon.setVisibility(View.GONE);
                    text.setVisibility(View.GONE);
                    for (DataSnapshot soloHistory : dataSnapshot.getChildren()) {
                        RecentHistory mRecentHistory = soloHistory.getValue(RecentHistory.class);
                        recentHistories.add(mRecentHistory);
                    }

                    recentHistoryAdapter = new RecentHistoryAdapter(HistoryActivity.this, recentHistories);
                    recyclerView.setAdapter(recentHistoryAdapter);

                }
                else {
                    recyclerView.setVisibility(View.GONE);
                    icon.setVisibility(View.VISIBLE); text.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
